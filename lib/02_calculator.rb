def add(num1, num2)
    num1 + num2
end

def subtract(num1, num2)
    num1 - num2
end

def sum(array)
    sum = 0
    array.each do |i|
        sum += i
    end
    return sum
end

def multiply(array)
    result = 1
    array.each do |i|
        result = result * i
    end
    return result
end

def power(num, exp)
    num**exp
end

def factorial(n)
    if n == 0
        return 1
    elsif n < 0
        return false
    end

    result = n
    i = 1
    while i < n
        result = result * i
        i += 1
    end
    return result
end
