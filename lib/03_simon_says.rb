def echo(string)
    string
end

def shout(string)
    string.upcase!
end

def repeat(string, tms=2)
    (string + " ") * (tms - 1) + string
end

def start_of_word(string, hm=1)
    string[0, hm]
end

def first_word(string)
    array = string.split
    array[0]
end

def titleize(string)
    array = string.split(' ')
    array.each do |i|
        i.capitalize! unless (i == "and") || (i == "the") || (i == "over")
    end
    array[0].capitalize!
    array.join(' ')
end
