def translate(string)
  vowels = ["a", "e", "i", "o", "u"]
  e = "ay"
  array = string.split(' ')
  new_array = []

  array.each do |word|

    c = 0
    if word[0] =~ /[A-Z]/
      c +=1
      word.downcase!
    end

    capifpush = Proc.new do |new_word|
      if c == 1
        new_word.capitalize!
      end
      if new_word =~ /[,.:;?!]/
        idx = new_word.index(/[,.:;?!]/)
        punc = new_word[idx]
        new_word.delete!(new_word[idx])
        new_word += punc
      end
      new_array.push(new_word)
    end

    case word[0]
      when /[aeiou]/
      new_word = word + e
      capifpush.call(new_word)
      when /[^AEIOUaeiou]/
        if !(vowels.include?(word[1])) && (word[1..2] == "qu")
          new_word = word + word[0..2] + e
          new_word[0..2] = ""
          capifpush.call(new_word)
        elsif !(vowels.include?(word[1]))
          if !(vowels.include?(word[2]))
            new_word = word + word[0..2] + e
            new_word[0..2] = ""
            capifpush.call(new_word)
            break
          end
          new_word = word + word[0..1] + e
          new_word[0..1] = ""
          capifpush.call(new_word)
        elsif word[0..1] == "qu"
          new_word = word + "qu" + e
          new_word[0..1] = ""
          capifpush.call(new_word)
        else
          new_word = word + word[0] + e
          new_word[0] = ""
          capifpush.call(new_word)
        end
      end
    end

  new_string = new_array.join(' ')
  return new_string
end
